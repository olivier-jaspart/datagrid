﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Using namespaces  
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace EssaiDatagrid
{
    /// <summary>  
    /// Interaction logic for MainWindow.xaml  
    /// </summary>  
    public partial class WindowPseudos : Window
    {
        #region MySqlConnection Connection  
        MySqlConnection conn = new
        MySqlConnection(ConfigurationManager.ConnectionStrings["Serveur"].ConnectionString);

     

        public WindowPseudos()
        {
            InitializeComponent();
        }
        #endregion
        #region bind data to DataGrid.  
        private void btnloaddata_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("Select Id_Pseudo,Pseudo_Prenom,Pseudo_Nom,Pseudo_Nom_Particule,Pseudo_Naissance,Pseudo_Deces,Pays_Nom,Image_Mini_Drapeau from tbl_pseudo,tbl_pays WHERE tbl_pseudo.Id_Pays = tbl_pays.Id_Pays LIMIT 20", conn);
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                dataGridPseudos.DataContext = ds;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //Finally {
            //    conn.Close();
            //}
        }
        #endregion
    }
}

