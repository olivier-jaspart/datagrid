﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EssaiDatagrid
{
    class Livres
    {
        private int id_Livre;
        private int id_Reference;
        private string livre_Numero;
        private string livre_Titre;
        private string livre_Sous_Titre;
        private int livre_Quantite;
        private int Id_Edition;
        private int Id_Collection;
        private string livre_Parution;
        private string livre_Cycle_nom1;
        private string livre_Cycle_Tome1;
        private string livre_Cycle_nom2;
        private string livre_Cycle_Tome2;
        private string livre_Cycle_nom3;
        private string livre_Cycle_Tome3;
        private string livre_infos;
        private int Id_format;
        private string livre_ISBN;
        private string livre_EAN;
        private string livre_Code_Prix;
        private int livre_Pages;
        private string livre_Couverture;
        private int Id_Langue;
        private int livre_Appreciation;
        private Boolean livre_Controle;
        private string livre_Titre_Original;
        private string livre_Date_Creation;
        private int Id_Etat;
        private int Id_Statut;
        private string livre_Details;
        private DateTime livre_Date_Achat;
        private double livre_Prix_Achat;
        private double livre_Cote;
        private string livre_Info_Achat;
        private Boolean livre_Lu;
        private DateTime livre_Date_Modification;
        private int Id_Version;
        private int Id_Logo;
        private Boolean livre_Nouveau_format;
        private string notice_BNF;
        private Boolean livre_Absence_Couverture;
        private int Id_Stockage_Images;
        private Boolean livre_Fiche_A_Completer;
        private Boolean livre_Fiche_OK;
        private string numero_notice;
        private int livre_Num_Tri;
        private DateTime livre_Date_Parution;


    }
}
