﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Using namespaces  
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace EssaiDatagrid
{

    

    public partial class WindowLivres : Window
    {
       
        MySqlConnection conn = new
        MySqlConnection(ConfigurationManager.ConnectionStrings["Serveur"].ConnectionString);
        private string requete;


        public WindowLivres()
        {
            InitializeComponent();
        }
       
        private void btnloaddata_Click(object sender, RoutedEventArgs e)
        {

            // Les 20 premiers livres de la base
            // requete = "Select Id_Livre,Livre_Titre,Livre_Numero,Livre_Parution,Collection_Nom,Edition_Nom from tbl_livre,tbl_collection,tbl_edition WHERE tbl_livre.Id_Collection = tbl_collection.Id_Collection and tbl_livre.Id_Edition = tbl_edition.Id_Edition LIMIT 20";
            // Tous les livres dont le titre contient "Maison"
            // requete = "Select Id_Livre,Livre_Titre,Livre_Numero,Livre_Parution,Collection_Nom,Edition_Nom from tbl_livre,tbl_collection,tbl_edition WHERE Livre_Titre LIKE '% Maison %' and tbl_livre.Id_Collection = tbl_collection.Id_Collection and tbl_livre.Id_Edition = tbl_edition.Id_Edition";
            // Tous les livres ou Edition_Nom = 'Fleuve Noir' et Collection_Nom = 'Anticipation' 
            requete = "Select Id_Livre,Livre_Titre,Livre_Numero,Livre_Parution,Collection_Nom,Edition_Nom from tbl_livre,tbl_collection,tbl_edition WHERE Edition_Nom = 'Fleuve Noir' and Collection_Nom = 'Anticipation' and tbl_livre.Id_Collection = tbl_collection.Id_Collection and tbl_livre.Id_Edition = tbl_edition.Id_Edition";
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(requete, conn);
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                dataGridListeLivres.DataContext = ds;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //Finally {
            //    conn.Close();
            //}
        }
        
    }
}

